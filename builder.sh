#!/bin/bash

# Define variables, pay attention to pathing, yours may differ
SOURCE_PATH="/--your-path-here--/shareable-discord-bot" # Path to cloned repo
DOCKERFILE_PATH="$SOURCE_PATH/musicbot.dockerfile" # Path to dockerfile, by default inside repo
IMAGE_REPO_NAME="discord-music-bot"  # What ever you want to set your repo name to be
TAG="latest"

# Navigate to the source directory
cd $SOURCE_PATH

# Build the Docker image
sudo docker build -t $IMAGE_REPO_NAME:$TAG -f $DOCKERFILE_PATH .

# Navigate back to the original directory
cd -

