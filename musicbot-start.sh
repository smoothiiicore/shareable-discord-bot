#!/bin/bash

# Define the environment variables
TOKEN="your-token-here"
BOT_CHAT_NAME="your-bot-textchannel-name-here"
COMMAND_PREFIX="your-commandprefix-here"

# Run the Docker container
sudo docker run -d -e TOKEN=$TOKEN -e BOT_CHAT_NAME=$BOT_CHAT_NAME -e COMMAND_PREFIX=$COMMAND_PREFIX discord-music-bot:latest
