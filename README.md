I probably have some form of dyslexia, typos are imminent

Short instructions on how to get this up and running:

## Clone repo and set your variables

There are few variables you need to setup.

Firstly you need to decide if you want to use the dockerized version. You can do this thing from command line, you just need to hardcode few things:

```
#########################################################################################################
################################### Manual variables ####################################################
# If you want to use this code from commandline
# Token for your bot
#TOKEN = 'your-token-here'
# Allowed text channel for bot to listen to
#ALLOWED_CHANNEL_NAME = 'ypur-bot-channel-name-here'
# Command prefix which your bot will follow
#COMMAND_PREFIX = 'your-chosen-prefix-here'
#########################################################################################################
#########################################################################################################





#########################################################################################################
################################### Dockerized variables ################################################
# Token, bot-channel name and command prefix as env variables(for docker)
# Token for your bot
TOKEN = os.getenv('TOKEN')
# Allowed text channel for bot to listen to
ALLOWED_CHANNEL_NAME = os.getenv('BOT_CHAT_NAME')
# Command prefix which your bot will follow
COMMAND_PREFIX = os.getenv('COMMAND_PREFIX')
#########################################################################################################
#########################################################################################################
```

Comment/Uncomment from the code how ever you want. By default its in docker version.

builder.sh variables:
```
# Define variables, pay attention to pathing, yours may differ
SOURCE_PATH="/--your-path-here--/shareable-discord-bot" # Path to cloned repo
DOCKERFILE_PATH="$SOURCE_PATH/musicbot.dockerfile" # Path to dockerfile, by default inside repo
IMAGE_REPO_NAME="discord-music-bot"  # What ever you want to set your repo name to be
TAG="latest"
```

Point $SOURCE_PATH into the repo, where ever you cloned it into. If you want to change image repo/tag names, go ahead.


Next up bot variables from musicbot-start.sh:
```
# Define the environment variables
TOKEN="your-token-here"
BOT_CHAT_NAME="your-bot-textchannel-name-here"
COMMAND_PREFIX="your-commandprefix-here"
```

Static token for your bot, text-channel your bot will be getting its commands from & lastly prefix for your commands. For example "!", ie "!help"



## Make image and run

After variables are all done, run builder.sh. You should now have an image. If you have an image, then run musicbot-start.sh

Gz, you should now have a youtube-able music bot, go nuts.
