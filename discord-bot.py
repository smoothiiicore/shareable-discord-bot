import discord
from discord.ext import commands
import yt_dlp
import asyncio
import re
from discord.ext.commands import DefaultHelpCommand
import random
import os


#########################################################################################################
################################### Manual variables ####################################################
# If you want to use this code from commandline
# Token for your bot
#TOKEN = 'your-token-here'
# Allowed text channel for bot to listen to
#ALLOWED_CHANNEL_NAME = 'ypur-bot-channel-name-here'
# Command prefix which your bot will follow
#COMMAND_PREFIX = 'your-chosen-prefix-here'
#########################################################################################################
#########################################################################################################





#########################################################################################################
################################### Dockerized variables ################################################
# Token, bot-channel name and command prefix as env variables(for docker)
# Token for your bot
TOKEN = os.getenv('TOKEN')
# Allowed text channel for bot to listen to
ALLOWED_CHANNEL_NAME = os.getenv('BOT_CHAT_NAME')
# Command prefix which your bot will follow
COMMAND_PREFIX = os.getenv('COMMAND_PREFIX')
#########################################################################################################
#########################################################################################################



# Create a bot instance
intents = discord.Intents.default()
intents.messages = True
intents.message_content = True
intents.voice_states = True
bot = commands.Bot(command_prefix=COMMAND_PREFIX, intents=intents)


class CustomHelpCommand(DefaultHelpCommand):
    def get_command_signature(self, command):
        return f'!{command.qualified_name} {command.signature}'

    async def send_bot_help(self, mapping):
        ctx = self.context
        help_message = "**Available Commands:**\n"
        for cog, commands in mapping.items():
            for command in commands:
                help_message += f"**{self.get_command_signature(command)}**: {command.help}\n"
        await ctx.send(help_message)

bot.help_command = CustomHelpCommand()




# Initialize some variables
song_queue = []
current_song = None
start_time = None


# Function to extract audio URL using yt-dlp
def get_audio_info(url):
    ydl_opts = {
        'format': 'bestaudio/best',
        'noplaylist': True,
        'quiet': True
    }
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(url, download=False)
        audio_info = {
            'url': info['url'],
            'video_id': info.get('id', 'Unknown ID'),
            'title': info.get('title', 'Unknown Title'),
            'duration': info.get('duration', 0),  # Duration in seconds
            'uploader': info.get('uploader', 'Unknown Uploader')
        }
    return audio_info


# Function to search youtube based on given string
def search_youtube(query):
    ydl_opts = {
        'format': 'bestaudio/best',
        'noplaylist': True,
        'quiet': True,
        'default_search': 'ytsearch',
        'extract_flat': 'in_playlist'
    }
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(query, download=False)
        video_url = info['entries'][0]['url']
    return video_url


# Command to join the voice channel
@bot.command()
async def join(ctx):
    """Join the voice channel you are currently in."""
    if not ctx.message.author.voice:
        await ctx.send("You are not in a voice channel!")
        return
    else:
        channel = ctx.message.author.voice.channel
        await channel.connect()


# Command to leave the voice channel
@bot.command()
async def disconnect(ctx):
    """Leave the voice channel."""
    if ctx.voice_client:
        await ctx.guild.voice_client.disconnect()
        global song_queue
        song_queue = []  # Clear the queue when leaving


# Check if the URL is valid
def is_youtube_url(url):
    youtube_regex = re.compile(
        r'^(https?://)?(www\.)?(youtube|youtu|youtube-nocookie)\.(com|be)/.+$'
    )
    return youtube_regex.match(url) is not None

# Command to play audio from a YouTube video
@bot.command()
async def play(ctx, *, query: str):
    """Play audio from a YouTube URL or search by song name."""
    if not ctx.voice_client:
        if ctx.author.voice:
            channel = ctx.author.voice.channel
            await channel.connect()
        else:
            await ctx.send("You are not in a voice channel and the bot is not in a voice channel!")
            return

    # Determine if the query is a URL or a search term
    if is_youtube_url(query):
        video_url = query
    else:
        video_url = search_youtube(query)

    # Use the same logic as before to play the song
    global song_queue
    song_info = get_audio_info(video_url)
    song_queue.append(song_info)
    await ctx.send(f"Song added to queue: {song_info['title']} by {song_info['uploader']}")

    if not ctx.voice_client.is_playing():
        await play_next_song(ctx)


# Command to stop playing audio and clear the queue
@bot.command()
async def stop(ctx):
    """Stop playing audio and clear the queue."""
    if ctx.voice_client:
        ctx.voice_client.stop()  # Stop the currently playing audio
        global song_queue
        song_queue = []  # Clear the queue
        await ctx.send("Stopped playing and cleared the queue.")


# Command to pause the playback
@bot.command()
async def pause(ctx):
    """Pause the currently playing audio."""
    if ctx.voice_client and ctx.voice_client.is_playing():
        ctx.voice_client.pause()  # Pause the currently playing audio
        await ctx.send("Paused the playback.")

# Command to resuming playing audio from where it was paused
@bot.command()
async def resume(ctx):
    """Resume the paused audio."""
    if ctx.voice_client and ctx.voice_client.is_paused():
        ctx.voice_client.resume()  # Resume the paused audio
        await ctx.send("Resumed the playback.")
    elif not ctx.voice_client.is_playing() and song_queue:
        await play_next_song(ctx)


# Command to skip the current song and play the next one in the queue
@bot.command()
async def skip(ctx):
    """Skip the current song and play the next one in the queue."""
    if ctx.voice_client and ctx.voice_client.is_playing():
        ctx.voice_client.stop()  # Stop the currently playing audio
        await ctx.send("Skipped the current song.")


# Command to display the current song queue
@bot.command()
async def queue(ctx):
    """Display the current song queue."""
    global song_queue
    if song_queue:
        queue_list = '\n'.join([f"{index + 1}. {song['title']}, uploaded by {song['uploader']} - {song['duration']//60}:{song['duration']%60:02d}" for index, song in enumerate(song_queue)])
        await ctx.send(f"Current queue:\n{queue_list}")
    else:
        await ctx.send("The queue is empty.")

# Removes given song number from queue
@bot.command()
async def remove(ctx, index: int):
    """Remove a song from the queue by its index."""
    global song_queue
    if index < 1 or index > len(song_queue):
        await ctx.send("Invalid index. Please provide a valid song number from the queue.")
        return

    removed_song = song_queue.pop(index - 1)
    await ctx.send(f"Removed **{removed_song['title']}** by **{removed_song['uploader']}** from the queue.")

# Shuffle all songs in the current queue
@bot.command()
async def shuffle(ctx):
    """Shuffle the songs in the queue."""
    global song_queue
    if len(song_queue) > 1:
        random.shuffle(song_queue)
        await ctx.send("The queue has been shuffled.")
        
        # Display the shuffled queue
        queue_list = '\n'.join([f"{index + 1}. {song['title']} by {song['uploader']} - {song['duration']//60}:{song['duration']%60:02d}" for index, song in enumerate(song_queue)])
        await ctx.send(f"Shuffled queue:\n{queue_list}")
    else:
        await ctx.send("Not enough songs in the queue to shuffle.")


# Exports current queue as a single string. String contain videoIDs separated by column
@bot.command()
async def export_queue(ctx):
    """Export the video IDs of songs in the queue as a single string."""
    global song_queue
    if song_queue:
        video_ids = ','.join([song['video_id'] for song in song_queue])
        await ctx.send(f"Exported video IDs:\n{video_ids}")
    else:
        await ctx.send("The queue is empty.")


# Function to get video info by ID. Used by !import_queue command
def get_audio_info_by_id(video_id):
    ydl_opts = {
        'format': 'bestaudio/best',
        'quiet': True
    }
    url = f"https://www.youtube.com/watch?v={video_id}"
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(url, download=False)
        audio_info = {
            'url': info['url'],
            'video_id': info.get('id', 'Unknown ID'),
            'title': info.get('title', 'Unknown Title'),
            'duration': info.get('duration', 0),  # Duration in seconds
            'uploader': info.get('uploader', 'Unknown Uploader')
        }
    return audio_info

# Import video IDs into the queue and start playing if not already playing
@bot.command()
async def import_queue(ctx, *, video_ids: str):
    """Import video IDs into the queue and start playing if not already playing."""
    global song_queue
    # Check if the user is in a voice channel
    if not ctx.author.voice:
        await ctx.send("You are not in a voice channel!")
        return
    # Join the user's voice channel if not already connected
    if not ctx.voice_client:
        channel = ctx.author.voice.channel
        await channel.connect()
    video_id_list = video_ids.split(',')
    for video_id in video_id_list:
        song_info = get_audio_info_by_id(video_id)
        song_queue.append(song_info)
    await ctx.send("Imported video IDs into the queue.")
    # Start playing if not already playing
    if not ctx.voice_client.is_playing():
        await play_next_song(ctx)







# Responsible for playing music after song has ended, being called from on_song_end event function
async def play_next_song(ctx, retries=3):
    global song_queue, current_song, start_time
    if len(song_queue) > 0:
        current_song = song_queue.pop(0)  # Remove the song from the queue before playing
        audio_url = current_song['url']  # Use the audio URL from the song info
        start_time = asyncio.get_event_loop().time()  # Store the start time

        ffmpeg_options = {
            'options': '-vn',
            'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5'
        }
        source = discord.FFmpegPCMAudio(audio_url, **ffmpeg_options)
        ctx.voice_client.play(source, after=lambda e: on_song_end(ctx, e, current_song, retries))

        await ctx.send(f"Now playing: {current_song['title']} by {current_song['uploader']}")
    else:
        # Reset current_song and start_time when the queue is empty
        current_song = None
        start_time = None
        await ctx.send("Queue is empty")





async def handle_playback_error(ctx, error, url, retries):
    if error:
        print(f"Playback error: {error}")
        if retries > 0:
            print(f"Retrying... {retries} attempts left")
            await play_next_song(ctx, retries - 1)
        else:
            await ctx.send("Failed to play the song after multiple attempts.")
    else:
        await play_next_song(ctx)


def on_song_end(ctx, error, song_info, retries):
    if error:
        asyncio.run_coroutine_threadsafe(handle_playback_error(ctx, error, song_info, retries), bot.loop)
    else:
        asyncio.run_coroutine_threadsafe(play_next_song(ctx), bot.loop)









@bot.command()
async def current(ctx):
    """Display information about the currently playing song, including a visual progress bar."""
    global current_song, start_time

    if current_song is None:
        await ctx.send("No song is currently playing.")
        return

    # Calculate elapsed time
    elapsed_time = asyncio.get_event_loop().time() - start_time
    elapsed_seconds = int(elapsed_time)
    elapsed_minutes = elapsed_seconds // 60
    elapsed_seconds %= 60

    # Calculate total duration
    total_duration = current_song['duration']
    remaining_time = total_duration - elapsed_time
    remaining_seconds = int(remaining_time)
    remaining_seconds %= 60

    # Generate progress bar
    total_bars = 20
    filled_bars = int((elapsed_time / total_duration) * total_bars)
    progress_bar = f"{'█' * filled_bars}{'-' * (total_bars - filled_bars)}"

    # Format the message
    message = (
        f"**Title:** {current_song['title']}\n"
        f"**Progress:** [{progress_bar}] {elapsed_minutes}:{elapsed_seconds:02d} / {total_duration // 60}:{total_duration % 60:02d}"
    )

    await ctx.send(message)






@bot.event
async def on_message(message):
    # Check if the message is in the allowed text channel
    if message.channel.name != ALLOWED_CHANNEL_NAME:
        return
    await bot.process_commands(message)


@bot.event
async def on_voice_state_update(member, before, after):
    global song_queue
    # Check if the bot is disconnected from a voice channel
    if before.channel is not None and after.channel is None and member == bot.user:
        song_queue = []  # Clear the queue if the bot is disconnected
        print("Bot disconnected from voice channel, queue cleared.")
    # Check if the bot is in a voice channel
    voice_channel = member.guild.voice_client
    if voice_channel is not None:
        # Check if there are no members in the voice channel (excluding the bot)
        if len(voice_channel.channel.members) == 1:
            print("No members left in the voice channel, starting 2 minute timer.")
            # If no users are left, schedule a timer to disconnect the bot after 2 minutes
            await asyncio.sleep(120)  # Wait for 2 minutes
            if len(voice_channel.channel.members) == 1:
                # Check again if there are still no members, disconnect (excluding the bot)
                await voice_channel.disconnect()
                print("Disconnected bot due to inactivity.")


# Run the bot
bot.run(TOKEN)

